## Mastering Containers and Kubernetes.
## Duration: 50 hrs
## Total Classes: 20 ,2.5hrs each on weekend 
## Timings: 9 AM to 12 PM 
## Fee:      8999 INR
## Max Participants: 20
## Payment Details:  Gpay/Paytm 9999473568
###  Recordings and Labs will also be provided during sessions.

### Topics to be Covered.


Containers:
 - Introduction to Containers
 - Introduction to  Docker
 - Introduction to Podman
 - Introuction to Crun,Crio
 - Container Networking
 - Container storage
 - Container Image Management 
 - Docker Compose 
 - Container security 

Kubernetes:
  - Introduction  to kuberntes 
  - Set up kuberntes on Linux VMS
  - Setting up Kubernetes on AZURE/AWS/GCP
  - Kubernetes Volume Management 
    - Storage Class
    - Ceph Storage 
  - Kubernetes Backup and Disaster Recovery 
     - Kasten
     - Velero
  - Kubernetes  resource cleanup
    - Tool reduce CO2 footprint of your clusters
  - Kubernetes Native Policy Management
    - Kyverno
  - Kubernetes Security and Compliance Management 
     - neuvector

  - Kubernetes platform tailored for hybrid multicloud
    - Kube Sphere
    - kube lens

  - Virtualization using kubernetes Api
    - Kubevirt
  - Service Mesh
    - Istio
  - Kubernetes Certificate manager
    - cert-manager


  Introduction to Openshift 
